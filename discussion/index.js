// [SECTION] Create an API Server using Express

// Identify the proper ingredient/materials/components needed to start the project.

// Use the 'require' directive to load the express module/package
// express => this will allow us to access methods and functions that will make creating a server easier.
const express = require('express');

// Create an application using express.
// express() -> this creates an express/instant application and we will give an identifier for the app it will produce.
// In lay man's term, the application will be our server.
const application = express();

// Identify a virtual port in which to serve the project.
const port = 4000;

// Assign the established connection/server into the designated port.
application.listen(port, () => console.log(`Server is running on port ${port}`));